
//
//  MyExtension.swift
//  20160505_iOSDay9_Alert
//
//  Created by ChenSean on 5/5/16.
//  Copyright © 2016 ChenSean. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    // 延伸 UIViewController 可以方便做 Alert 等原本 ViewController 裡面的東西
    func showMyAlert(message: String){
        let alert = UIAlertController(title: "警告", message: message, preferredStyle: .Alert)
        // 至少給 alert 一個按鈕可以關掉
        let okAction = UIAlertAction(title: "Confirm", style: .Default) { (action) in
            // 點擊 action 後要執行的事情
            // 大括號這個是 clouser 寫法
            self.dismissViewControllerAnimated(true, completion: nil)
        }
        alert.addAction(okAction)
        presentViewController(alert, animated: true, completion: nil)
    }
}

